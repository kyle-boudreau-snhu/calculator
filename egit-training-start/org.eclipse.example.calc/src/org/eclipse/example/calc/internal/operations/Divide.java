package org.eclipse.example.calc.internal.operations;

import org.eclipse.example.calc.BinaryOperation;

/**
 * Binary minus operation
 */
public class Divide extends AbstractOperation implements BinaryOperation {

	@Override
	public float perform(float arg1, float arg2) {
		if(arg1 != 0 && arg2 != 0) {
			return arg1 / arg2;
		} else {
			// cannot divide by zero
			return 0;
		}
	}

	@Override
	public String getName() {
		return "/";
	}

}
