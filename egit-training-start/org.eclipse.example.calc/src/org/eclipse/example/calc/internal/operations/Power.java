package org.eclipse.example.calc.internal.operations;

import org.eclipse.example.calc.BinaryOperation;

/**
 * Binary plus operation
 */
public class Power extends AbstractOperation implements BinaryOperation {

	@Override
	public float perform(float arg1, float arg2) {
		float num = arg1;
		for(int i = 0; i < arg2 - 1; i++) {
			num *= arg1;
		}
		return num;
	}

	@Override
	public String getName() {
		return "^";
	}

}
